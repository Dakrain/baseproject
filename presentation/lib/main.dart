import 'package:base_project/src/config/styles/style.dart';
import 'package:base_project/src/di/injection.dart';
import 'package:base_project/src/modules/home/home_model.dart';
import 'package:base_project/src/modules/home/home_screen.dart';
import 'package:base_project/src/routes/app_router_observer.dart';
import 'package:data/data.dart';
import 'package:domain/domain.dart';
import 'package:domain/usecases/movie_usecase.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import 'generated/l10n.dart';
import 'src/routes/app_router.gr.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  //Inject dependency
  await Injection.inject();

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

final appRouter = AppRouter();

class _MyAppState extends State<MyApp> {
  Locale _locale = Locale('vi');

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(414, 896),
      builder: () => MaterialApp.router(
        title: 'Flutter Demo',
        theme: AppTheme.lightTheme,
        localizationsDelegates: const [
          S.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        themeMode: AppTheme().init(),
        supportedLocales: S.delegate.supportedLocales,
        locale: _locale,
        routerDelegate:
            appRouter.delegate(navigatorObservers: () => [AppRouterObserver()]),
        routeInformationParser: appRouter.defaultRouteParser(),
      ),
    );
  }
}
