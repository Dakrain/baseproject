import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:base_project/main.dart';
import 'package:base_project/src/di/injection.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

enum ToastType { warning, error, info, success }

class AppPopup {
  static void showToast(
    String message, {
    ToastType toastType = ToastType.info,
    ToastGravity gravity = ToastGravity.CENTER,
  }) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: gravity,
        fontSize: 14);
  }

  static void showAppDialog(
    String message, {
    String? title,
    String? confirm,
    DialogType dialogType = DialogType.INFO,
  }) {
    AwesomeDialog(
      context: appRouter.navigatorKey.currentContext!,
      title: title,
      desc: message,
      dialogType: dialogType,
    )..show();
  }
}
