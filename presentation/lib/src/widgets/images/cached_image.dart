import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';

class CachedNetworkImage extends StatelessWidget {
  const CachedNetworkImage({
    Key? key,
    required this.imageUrl,
    this.width,
    this.height,
  }) : super(key: key);
  final String imageUrl;
  final double? width;
  final double? height;

  @override
  Widget build(BuildContext context) {
    return ExtendedImage.network(
      imageUrl,
      fit: BoxFit.cover,
      enableLoadState: false,
      cache: true,
      width: width,
      height: height,
      loadStateChanged: (ExtendedImageState state) {
        if (state.extendedImageLoadState == LoadState.loading) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (state.extendedImageLoadState == LoadState.failed) {
          return Center(
            child: Icon(
              Icons.error_rounded,
              color: Colors.red,
            ),
          );
        }
      },
    );
  }
}
