import 'package:base_project/src/di/injection.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'base_model.dart';

class BaseView<T extends BaseModel> extends StatefulWidget {
  final Widget Function(
    BuildContext context,
    T model,
    Widget? child,
  ) builder;

  ///This call back is call once the view is ready.
  final Function(T)? onModelReady;

  BaseView({
    required this.builder,
    this.onModelReady,
  });

  @override
  _BaseViewState<T> createState() => _BaseViewState<T>();
}

class _BaseViewState<T extends BaseModel> extends State<BaseView<T>> {
  ///We register the model with Get_it, so every time model call
  ///we register a new instance.
  T model = injector<T>();

  @override
  void initState() {
    if (widget.onModelReady != null) {
      widget.onModelReady?.call(model);
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return ChangeNotifierProvider<T>(
      create: (context) => model,
      child: Consumer<T>(
        builder: widget.builder,
      ),
    );
  }
}
