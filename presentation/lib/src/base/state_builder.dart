import 'package:base_project/src/base/base_model.dart';

import 'package:flutter/material.dart';

class StateBuilder extends StatelessWidget {
  const StateBuilder(this.state,
      {Key? key,
      required this.onLoadingState,
      required this.onErrorState,
      required this.onIdleState})
      : super(key: key);

  final ViewState state;

  final Widget Function(BuildContext context) onLoadingState;
  final Widget Function(BuildContext context) onErrorState;
  final Widget Function(BuildContext context) onIdleState;

  @override
  Widget build(BuildContext context) {
    switch (state) {
      case ViewState.Busy:
        return onLoadingState(context);

      case ViewState.Error:
        return onErrorState(context);

      default:
        return onIdleState(context);
    }
  }
}
