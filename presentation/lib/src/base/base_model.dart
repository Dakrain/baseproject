import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:base_project/src/utils/app_pop.dart';
import 'package:data/data.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

/// Represents the state of the view
enum ViewState { Idle, Busy, Error, Loadmore }

class BaseModel extends ChangeNotifier {
  ViewState _state = ViewState.Idle;
  ServerError? _serverError;

  ViewState get state => _state;

  String? get errorMessage => _serverError?.message;
  String? get errorCode => _serverError?.code.toString();

  void setState(ViewState state) {
    _state = state;
    notifyListeners();
  }

  void showAppDialog(String message,
      {String? title,
      String? confirm,
      DialogType dialogType = DialogType.INFO}) {
    AppPopup.showAppDialog(message,
        title: title, confirm: confirm, dialogType: dialogType);
  }

  void showToast(
    String message, {
    ToastType toastType = ToastType.info,
    ToastGravity gravity = ToastGravity.CENTER,
  }) {
    AppPopup.showToast(
      message,
      toastType: toastType,
      gravity: gravity,
    );
  }
}
