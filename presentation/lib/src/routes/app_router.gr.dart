// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:auto_route/auto_route.dart' as _i1;
import 'package:domain/domain.dart' as _i6;
import 'package:flutter/material.dart' as _i2;

import '../modules/detail_movie/detail_movie_screen.dart' as _i4;
import '../modules/home/home_screen.dart' as _i3;
import '../modules/unknown_page/unknow_page.dart' as _i5;

class AppRouter extends _i1.RootStackRouter {
  AppRouter([_i2.GlobalKey<_i2.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i1.PageFactory> pagesMap = {
    HomeScreenRoute.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (_) {
          return const _i3.HomeScreen();
        }),
    DetailMovieScreenRoute.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (data) {
          final args = data.argsAs<DetailMovieScreenRouteArgs>();
          return _i4.DetailMovieScreen(
              key: args.key, movieModel: args.movieModel);
        }),
    UnknownPageRoute.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (_) {
          return const _i5.UnknownPage();
        })
  };

  @override
  List<_i1.RouteConfig> get routes => [
        _i1.RouteConfig('/#redirect',
            path: '/', redirectTo: '/home', fullMatch: true),
        _i1.RouteConfig(HomeScreenRoute.name, path: '/home'),
        _i1.RouteConfig(DetailMovieScreenRoute.name, path: '/detail'),
        _i1.RouteConfig(UnknownPageRoute.name, path: '*')
      ];
}

class HomeScreenRoute extends _i1.PageRouteInfo<void> {
  const HomeScreenRoute() : super(name, path: '/home');

  static const String name = 'HomeScreenRoute';
}

class DetailMovieScreenRoute
    extends _i1.PageRouteInfo<DetailMovieScreenRouteArgs> {
  DetailMovieScreenRoute({_i2.Key? key, required _i6.MovieModel movieModel})
      : super(name,
            path: '/detail',
            args: DetailMovieScreenRouteArgs(key: key, movieModel: movieModel));

  static const String name = 'DetailMovieScreenRoute';
}

class DetailMovieScreenRouteArgs {
  const DetailMovieScreenRouteArgs({this.key, required this.movieModel});

  final _i2.Key? key;

  final _i6.MovieModel movieModel;
}

class UnknownPageRoute extends _i1.PageRouteInfo<void> {
  const UnknownPageRoute() : super(name, path: '*');

  static const String name = 'UnknownPageRoute';
}
