import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'dart:developer' as dev;

class AppRouterObserver extends AutoRouteObserver {
  @override
  void didPush(Route route, Route? previousRoute) {
    super.didPush(route, previousRoute);
    dev.log('New route pushed: ${route.settings.name}', name: 'AutoRoute');
  }

  @override
  void didPop(Route route, Route? previousRoute) {
    super.didPop(route, previousRoute);
    dev.log('Pop route: ${route.settings.name}', name: 'AutoRoute');
  }

  @override
  void didRemove(Route route, Route? previousRoute) {
    super.didRemove(route, previousRoute);
    dev.log('Remove route: ${route.settings.name}', name: 'AutoRoute');
  }

  @override
  void didReplace({Route? newRoute, Route? oldRoute}) {
    super.didReplace(newRoute: newRoute, oldRoute: oldRoute);
    dev.log(
        'Replace: ${newRoute?.settings.name} from $oldRoute?.settings.name}',
        name: 'AutoRoute');
  }

  @override
  void didStartUserGesture(Route route, Route? previousRoute) {
    super.didStartUserGesture(route, previousRoute);
  }

  @override
  void didStopUserGesture() {
    super.didStopUserGesture();
  }
}
