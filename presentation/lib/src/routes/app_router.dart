import 'package:auto_route/annotations.dart';
import 'package:auto_route/auto_route.dart';
import 'package:base_project/src/modules/detail_movie/detail_movie_screen.dart';
import 'package:base_project/src/modules/home/home_screen.dart';
import 'package:base_project/src/modules/unknown_page/unknow_page.dart';

class RoutePaths {
  static const String _root = '/';
  static const String splash = _root + 'splash';
  static const String home = _root + 'home';
  static const String detail = _root + 'detail';
}

@MaterialAutoRouter(routes: [
  AutoRoute(
    page: HomeScreen,
    path: RoutePaths.home,
    initial: true,
  ),
  AutoRoute(
    page: DetailMovieScreen,
    path: RoutePaths.detail,
  ),
  AutoRoute(
    path: '*',
    page: UnknownPage,
  )
])
class $AppRouter {}

class AuthGuard extends AutoRouteGuard {
  @override
  void onNavigation(NavigationResolver resolver, StackRouter router) {
    //Implement authentication guard here
  }
}
