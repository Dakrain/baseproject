library style;

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

part 'app_colors.dart';
part 'app_text_style.dart';
part 'app_theme.dart';
