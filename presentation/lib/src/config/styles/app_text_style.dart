part of style;

TextStyle _textStyle = TextStyle(
  color: kGreyscale,
);

TextStyle kT48M = _textStyle.copyWith(
  fontSize: 48.0.sp,
  fontWeight: FontWeight.w500,
);
TextStyle kT48R = _textStyle.copyWith(
  fontSize: 48.0.sp,
  fontWeight: FontWeight.w400,
);

TextStyle kT30M = _textStyle.copyWith(
  fontSize: 30.0.sp,
  fontWeight: FontWeight.w500,
);
TextStyle kT30R = _textStyle.copyWith(
  fontSize: 30.0.sp,
  fontWeight: FontWeight.w400,
);

TextStyle kT24M = _textStyle.copyWith(
  fontSize: 24.0.sp,
  fontWeight: FontWeight.w500,
);
TextStyle kT24R = _textStyle.copyWith(
  fontSize: 24.0.sp,
  fontWeight: FontWeight.w400,
);

TextStyle kT20M = _textStyle.copyWith(
  fontSize: 20.0.sp,
  fontWeight: FontWeight.w500,
);
TextStyle kT20R = _textStyle.copyWith(
  fontSize: 20.0.sp,
  fontWeight: FontWeight.w400,
);

TextStyle kT16M = _textStyle.copyWith(
  fontSize: 16.0.sp,
  fontWeight: FontWeight.w500,
);
TextStyle kT16R = _textStyle.copyWith(
  fontSize: 16.0.sp,
  fontWeight: FontWeight.w400,
);

TextStyle kT14M = _textStyle.copyWith(
  fontSize: 14.0.sp,
  fontWeight: FontWeight.w500,
);
TextStyle kT14R = _textStyle.copyWith(
  fontSize: 14.0.sp,
  fontWeight: FontWeight.w400,
);

TextStyle kT12M = _textStyle.copyWith(
  fontSize: 12.0.sp,
  fontWeight: FontWeight.w500,
);
TextStyle kT12R = _textStyle.copyWith(
  fontSize: 12.0.sp,
  fontWeight: FontWeight.w400,
);

extension TextStyleVision on TextStyle {
  TextStyle get r => copyWith(color: Colors.red);

  TextStyle get wh => copyWith(color: Colors.white);

  TextStyle get bl => copyWith(color: Colors.black);

  TextStyle get bold => copyWith(fontWeight: FontWeight.w700);
}
