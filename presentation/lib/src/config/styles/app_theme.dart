part of style;

class AppTheme {
  static final String appFontFamily = 'Roboto';

  static final ThemeData lightTheme = ThemeData();

  static final ThemeData darkTheme = ThemeData();

  ThemeMode init() {
    //Insert database save user theme here
    return ThemeMode.light;
  }

  void changeThemeMode(ThemeMode themeMode) {
    //Change theme
  }
}
