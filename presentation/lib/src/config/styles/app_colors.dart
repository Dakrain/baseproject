part of style;

const Color kPrimaryColor = Color(0xffFF3C3C);
const Color kAlternateColorBlue = Color(0xff0064FF);
const Color kAlternateColorYellow = Color(0xffFFC83C);
const Color kAlternateColorOrange = Color(0xffFF7800);
const Color kAlternateColorGreen = Color(0xff1EC88C);
const Color kAlternateColorPurple = Color(0xff7850DC);
const Color kAlternateColorRed = Color(0xffFF5A5A);
const Color kAlternateColorLightBlue = Color(0xff50C8FF);

const Color kGreyscale = Color(0xff333333);
const Color kGreyscale3 = Color(0xffF7F7F7);
const Color kGreyscale5 = Color(0xffF2F2F2);
const Color kGreyscale50 = Color(0xff808080);
const Color kGreyscale20 = Color(0xffCCCCCC);
const Color kGreyscale10 = Color(0xffE5E5E5);
