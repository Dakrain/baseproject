import 'package:auto_route/auto_route.dart';
import 'package:base_project/src/base/base_view.dart';
import 'package:base_project/src/base/state_builder.dart';
import 'package:base_project/src/modules/home/home_model.dart';
import 'package:base_project/src/routes/app_router.dart';
import 'package:base_project/src/routes/app_router.gr.dart';
import 'package:base_project/src/utils/app_pop.dart';
import 'package:base_project/src/widgets/images/cached_image.dart';
import 'package:domain/domain.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseView<HomeModel>(
      onModelReady: (HomeModel model) {
        model.getPopularMovies();
      },
      builder: (context, model, child) => Scaffold(
          appBar: AppBar(
            title: Text('Home screen'),
          ),
          body: StateBuilder(model.state, onLoadingState: (context) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }, onErrorState: (context) {
            return Center(
              child: Text('Something went wrong'),
            );
          }, onIdleState: (context) {
            return SingleChildScrollView(
              child: GridView.builder(
                padding: EdgeInsets.all(10),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  mainAxisSpacing: 10,
                  crossAxisSpacing: 10,
                ),
                itemBuilder: (context, index) {
                  final MovieModel data = model.listPopularMovies[index];
                  return InkWell(
                    onTap: () {
                      AutoRouter.of(context)
                          .push(DetailMovieScreenRoute(movieModel: data));
                    },
                    child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Stack(
                          alignment: Alignment.bottomCenter,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(20),
                              child: Hero(
                                tag: data,
                                child: CachedNetworkImage(
                                  imageUrl:
                                      'https://image.tmdb.org/t/p/original' +
                                          data.posterPath!,
                                  width: ScreenUtil().screenWidth / 2,
                                  height: ScreenUtil().screenWidth / 2,
                                ),
                              ),
                            )
                          ],
                        )),
                  );
                },
                itemCount: model.listPopularMovies.length,
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
              ),
            );
          })),
    );
  }
}
