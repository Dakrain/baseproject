import 'package:base_project/src/base/base_model.dart';
import 'package:base_project/src/di/injection.dart';
import 'package:data/data.dart';
import 'package:domain/domain.dart';
import 'package:domain/usecases/movie_usecase.dart';

class HomeModel extends BaseModel {
  MovieUsecase get _movieUsecase => injector();
  List<MovieModel> listPopularMovies = [];

  Future<void> getPopularMovies() async {
    setState(ViewState.Busy);
    try {
      final data = await _movieUsecase.getPopularMovies();
      listPopularMovies = data;
    } on ServerError catch (e, stacktrace) {
      print(e.toString());
      print(stacktrace);
    } catch (e) {} finally {
      setState(ViewState.Idle);
    }
  }
}
