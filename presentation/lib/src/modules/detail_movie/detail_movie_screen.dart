import 'dart:ui';

import 'package:base_project/src/config/styles/style.dart';
import 'package:base_project/src/widgets/images/cached_image.dart';
import 'package:domain/domain.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DetailMovieScreen extends StatelessWidget {
  const DetailMovieScreen({Key? key, required this.movieModel})
      : super(key: key);
  final MovieModel movieModel;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          movieModel.title ?? 'Movie',
        ),
      ),
      body: Column(
        children: [
          SizedBox(
              width: ScreenUtil().screenWidth,
              height: ScreenUtil().screenHeight / 3,
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                      image: ExtendedImage.network(
                        'https://image.tmdb.org/t/p/original' +
                            movieModel.backdropPath!,
                      ).image,
                      fit: BoxFit.cover,
                    )),
                    child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
                      child: Container(
                        decoration: new BoxDecoration(
                            color: Colors.white.withOpacity(0.0)),
                      ),
                    ),
                  ),
                  Hero(
                    tag: movieModel,
                    child: CachedNetworkImage(
                      imageUrl: 'https://image.tmdb.org/t/p/original' +
                          movieModel.posterPath!,
                      width: 150.w,
                      height: 200.w,
                    ),
                  )
                ],
              )),
          Text(
            movieModel.title ?? 'Movie',
            style: kT48M,
          )
        ],
      ),
    );
  }
}
