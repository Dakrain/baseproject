part of injector;

class NetworkModule {
  static void inject() {
    //Network
    injector.registerLazySingleton<ApiService>(
        () => ApiService(Dio(BaseOptions(queryParameters: {
              'api_key': 'f25c1146c688beb2a9da27778827d65b',
            }))
              ..interceptors.add(LogInterceptor(
                requestBody: true,
                responseBody: true,
              ))));
  }
}
