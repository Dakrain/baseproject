part of injector;

class UsecasesModule {
  static void inject() {
    //Usecase
    injector.registerFactory<MovieUsecase>(() => MovieUsecase(injector()));
  }
}
