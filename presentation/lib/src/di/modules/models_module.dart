part of injector;

class ModelsModule {
  static void inject() {
    injector.registerFactory<HomeModel>(() => HomeModel());
  }
}
