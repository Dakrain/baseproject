part of injector;

class RepositoriesModule {
  static void inject() {
    //Database
    injector.registerLazySingleton<IDatabaseManager>(
        () => DatabaseManager()..initDatabase('somepath'));

    //Repository
    injector.registerLazySingleton<MovieRepository>(
        () => MovieRepositoryImpl(injector(), injector()));
  }
}
