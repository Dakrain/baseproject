library injector;

import 'package:base_project/src/modules/home/home_model.dart';
import 'package:data/data.dart';
import 'package:data/datasources/database/database_manager.dart';
import 'package:data/repository/movie_repository/movie_repository_impl.dart';
import 'package:dio/dio.dart';
import 'package:domain/repositories/movie_repository/movie_repository.dart';
import 'package:domain/usecases/movie_usecase.dart';
import 'package:get_it/get_it.dart';

part 'modules/models_module.dart';
part 'modules/network_module.dart';
part 'modules/repositories_module.dart';
part 'modules/usecase_module.dart';

final injector = GetIt.instance;

class Injection {
  static Future<void> inject() async {
    UsecasesModule.inject();
    RepositoriesModule.inject();
    ModelsModule.inject();
    NetworkModule.inject();
  }
}
