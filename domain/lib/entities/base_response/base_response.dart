import 'package:freezed_annotation/freezed_annotation.dart';

part 'base_response.freezed.dart';
part 'base_response.g.dart';

@JsonSerializable(genericArgumentFactories: true)
@freezed
class BaseResponse<T> with _$BaseResponse<T> {
  const factory BaseResponse({
    @JsonKey(name: 'page') int? page,
    @JsonKey(name: 'results') T? results,
  }) = _BaseResponse<T>;
  factory BaseResponse.fromJson(
      Map<String, dynamic> json, T Function(Object?) fromJsonT) {
    return _$BaseResponseFromJson(json, fromJsonT);
  }
}
