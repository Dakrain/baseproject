import 'package:domain/entities/movies/movie_model.dart';
import 'package:domain/repositories/movie_repository/movie_repository.dart';

class MovieUsecase {
  final MovieRepository _repository;

  const MovieUsecase(this._repository);

  Future<MovieModel> getLastestMovies() {
    return _repository.getLatestMovies();
  }

  Future<List<MovieModel>> getPopularMovies() {
    return _repository.getPopularMovies();
  }
}
