import 'package:domain/entities/movies/movie_model.dart';

abstract class MovieRepository {
  Future<MovieModel> getLatestMovies();
  Future<List<MovieModel>> getPopularMovies();
}
