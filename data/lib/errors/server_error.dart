import 'package:dio/dio.dart';

enum ErrorType {
  serverError,
  receiveTimeout,
  connectTimeout,
  sendTimeout,
  cancel,
  noNetwork,
  unKnown,
  other
}

class ServerError with Exception {
  ServerError({this.code, this.message, this.errorType});

  int? code;
  String? message;
  ErrorType? errorType;

  factory ServerError.fromDioError(DioError dioError) {
    ErrorType errorType = ErrorType.unKnown;
    int? code = dioError.response?.statusCode;

    switch (dioError.type) {
      case DioErrorType.connectTimeout:
        errorType = ErrorType.connectTimeout;
        break;
      case DioErrorType.sendTimeout:
        errorType = ErrorType.sendTimeout;

        break;
      case DioErrorType.receiveTimeout:
        errorType = ErrorType.receiveTimeout;

        break;
      case DioErrorType.response:
        errorType = ErrorType.serverError;

        break;
      case DioErrorType.cancel:
        errorType = ErrorType.cancel;

        break;
      case DioErrorType.other:
        errorType = ErrorType.other;
        break;
    }

    return ServerError(
      errorType: errorType,
      code: code,
    );
  }
}
