import 'package:dio/dio.dart';
import 'package:domain/domain.dart';

import 'package:retrofit/retrofit.dart';

part 'api_service.g.dart';

@RestApi(baseUrl: 'https://api.themoviedb.org/3/')
abstract class ApiService {
  factory ApiService(Dio dio, {String? baseUrl}) = _ApiService;

  @GET('movie/latest')
  Future<MovieModel> getLastestMovie();

  @GET('movie/popular')
  Future<BaseResponse<List<MovieModel>>> getPopularMovies();
}
