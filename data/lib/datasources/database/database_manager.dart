import 'package:hive/hive.dart';

abstract class IDatabaseManager {
  Future<void> initDatabase(String path);
}

class DatabaseManager extends IDatabaseManager {
  @override
  Future<void> initDatabase(String path) async {
    Hive..init(path);
  }
}
