import 'package:hive/hive.dart';

abstract class BaseDao<T, D> extends HiveObject {
  T fromHiveObject({D? instance});
  D toHiveObject({T? instance});
}
