import 'package:data/datasources/daos/base_dao.dart';
import 'package:domain/domain.dart';
import 'package:hive/hive.dart';

part 'movie_dao.g.dart';

@HiveType(typeId: 1)
class MovieDao extends BaseDao<MovieModel, MovieDao> {
  @HiveField(0)
  String? originalTitle;
  @HiveField(1)
  String? posterPath;
  @HiveField(2)
  String? releaseDate;
  @HiveField(3)
  String? title;
  @HiveField(4)
  String? overView;

  @override
  MovieModel fromHiveObject({MovieDao? instance}) {
    return MovieModel(
      originalTitle: instance?.originalTitle,
      posterPath: instance?.posterPath,
      title: instance?.title,
      overView: instance?.overView,
    );
  }

  @override
  MovieDao toHiveObject({MovieModel? instance}) {
    return MovieDao();
  }
}
