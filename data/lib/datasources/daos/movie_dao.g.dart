// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_dao.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class MovieDaoAdapter extends TypeAdapter<MovieDao> {
  @override
  final int typeId = 1;

  @override
  MovieDao read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return MovieDao()
      ..originalTitle = fields[0] as String?
      ..posterPath = fields[1] as String?
      ..releaseDate = fields[2] as String?
      ..title = fields[3] as String?
      ..overView = fields[4] as String?;
  }

  @override
  void write(BinaryWriter writer, MovieDao obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.originalTitle)
      ..writeByte(1)
      ..write(obj.posterPath)
      ..writeByte(2)
      ..write(obj.releaseDate)
      ..writeByte(3)
      ..write(obj.title)
      ..writeByte(4)
      ..write(obj.overView);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MovieDaoAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
