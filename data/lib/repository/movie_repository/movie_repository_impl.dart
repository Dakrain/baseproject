import 'package:data/datasources/database/database_manager.dart';
import 'package:data/errors/server_error.dart';
import 'package:data/services/api_service.dart';
import 'package:dio/dio.dart';
import 'package:domain/domain.dart';
import 'package:domain/entities/movies/movie_model.dart';
import 'package:domain/repositories/movie_repository/movie_repository.dart';

class MovieRepositoryImpl extends MovieRepository {
  final ApiService _apiService;
  final IDatabaseManager _databaseManager;

  MovieRepositoryImpl(this._apiService, this._databaseManager);

  @override
  Future<MovieModel> getLatestMovies() async {
    try {
      final MovieModel movieModel = await _apiService.getLastestMovie();
      return movieModel;
    } on DioError catch (e) {
      throw ServerError.fromDioError(e);
    } catch (e) {
      throw ServerError(errorType: ErrorType.other);
    }
  }

  @override
  Future<List<MovieModel>> getPopularMovies() async {
    try {
      final BaseResponse<List<MovieModel>> response =
          await _apiService.getPopularMovies();
      return response.results ?? [];
    } on DioError catch (e, stacktrace) {
      print(stacktrace);
      throw ServerError.fromDioError(e);
    } catch (e) {
      throw ServerError(errorType: ErrorType.other);
    }
  }
}
